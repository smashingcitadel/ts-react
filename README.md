## TS Demo

### Requirement

Add new NYT API key

> `cp .env.sample .env` and change value of `REACT_APP_NYT_CREDENTIAL` in `.env` file.


Installing all the dependencies of project:

> `yarn` or `yarn install` then you are ready to start with `yarn start`


Testing

> `yarn test`


Thank you!