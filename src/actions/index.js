import {
  REQUEST_ARTICLES,
  RECEIVE_ARTICLES,
  SELECT_PAGE,
  INVALIDATE_PAGE
} from '../constants/ActionTypes'

export const selectPage = page => ({
  type: SELECT_PAGE,
  page
})

export const invalidatePage = page => ({
  type: INVALIDATE_PAGE,
  page
})

export const requestArticles = page => ({
  type: REQUEST_ARTICLES,
  page
})

export const receiveArticles = (page, json) => ({
  type: RECEIVE_ARTICLES,
  page,
  articles: json.response.docs,
  receivedAt: Date.now()
})

const fetchArticles = page => dispatch => {
  dispatch(requestArticles(page))
  return fetch(`https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=${process.env.REACT_APP_NYT_CREDENTIAL}&fl=headline,snippet,multimedia,pub_date,source&sort=newest&page=${page|0}`)
    .then(response => response.json())
    .then(json => dispatch(receiveArticles(page, json)))
}

const shouldFetchArticles = (state, page) => {
  const articles = state.articlesByPage[page]
  if (!articles) {
    return true
  }
  if (articles.isFetching) {
    return false
  }
  return articles.didInvalidate
}

export const fetchArticlesIfNeeded = page => (dispatch, getState) => {
  if (shouldFetchArticles(getState(), page)) {
    return dispatch(fetchArticles(page))
  }
}
