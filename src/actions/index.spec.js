import * as actions from '../constants/ActionTypes'
import * as articlesAction from './index'

describe('article actions', () => {
  it('selectPage should create SELECT_PAGE action', () => {
    expect(articlesAction.selectPage(0)).toEqual({
      type: actions.SELECT_PAGE,
      page: 0
    })
  })

  it('invalidatePage should create INVALIDATE_PAGE action', () => {
    expect(articlesAction.invalidatePage(0)).toEqual({
      type: actions.INVALIDATE_PAGE,
      page: 0
    })
  })

  it('requestArticles should create REQUEST_ARTICLES action', () => {
    expect(articlesAction.requestArticles(0)).toEqual({
      type: actions.REQUEST_ARTICLES,
      page: 0
    })
  })

  it('receiveArticles should create RECEIVE_ARTICLES action', () => {
    const data = {response: {docs: {}}}
    const date = Date.now()
    expect(articlesAction.receiveArticles(0, data)).toEqual({
      type: actions.RECEIVE_ARTICLES,
      page: 0,
      articles: data.response.docs,
      receivedAt: date
    })
  })
})
