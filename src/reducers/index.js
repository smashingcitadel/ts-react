import { combineReducers } from 'redux'
import {
  SELECT_PAGE,
  INVALIDATE_PAGE,
  REQUEST_ARTICLES,
  RECEIVE_ARTICLES
} from '../constants/ActionTypes'

const selectedPage = (state = '0', action) => {
  switch (action.type) {
    case SELECT_PAGE:
      return action.page
    default:
      return state
  }
}

const articles = (state = {
  isFetching: false,
  didInvalidate: false,
  items: []
}, action) => {
  switch (action.type) {
    case INVALIDATE_PAGE:
      return {
        ...state,
        didInvalidate: true
      }
    case REQUEST_ARTICLES:
      return {
        ...state,
        isFetching: true,
        didInvalidate: false
      }
    case RECEIVE_ARTICLES:
      return {
        ...state,
        isFetching: false,
        didInvalidate: false,
        items: action.articles
      }
    default:
      return state
  }
}

const articlesByPage = (state = { }, action) => {
  switch (action.type) {
    case INVALIDATE_PAGE:
    case RECEIVE_ARTICLES:
    case REQUEST_ARTICLES:
      return {
        ...state,
        [action.page]: articles(state[action.page], action)
      }
    default:
      return state
  }
}

const rootReducer = combineReducers({
  articlesByPage,
  selectedPage
})

export default rootReducer
