import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { selectPage, fetchArticlesIfNeeded } from '../actions'

import Pagination from '../components/Pagination'
import Articles from '../components/Articles'

import {
  Alert,
  Container,
  Row,
  Col,
  Navbar,
  NavbarToggler,
  NavbarBrand
} from 'reactstrap'

class App extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {isOpen: false}
  }

  static propTypes = {
    selectedPage: PropTypes.string.isRequired,
    articles: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  componentDidMount() {
    const { dispatch, selectedPage } = this.props
    dispatch(fetchArticlesIfNeeded(selectedPage))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedPage !== this.props.selectedPage) {
      const { dispatch, selectedPage } = nextProps
      dispatch(fetchArticlesIfNeeded(selectedPage))
    }
  }

  toggle() {
    this.setState({isOpen: !this.state.isOpen})
  }

  handleChange = nextPage => {
    this.props.dispatch(selectPage(nextPage))
    this.active = true
  }

  render() {
    const { selectedPage, articles, isFetching } = this.props
    const isEmpty = articles.length === 0
    return (
      <div>
        <Navbar color="light" expand="md">
          <Container>
            <NavbarBrand href="/">NYT</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
          </Container>
        </Navbar>

        <Container>
          {isEmpty
            ? (isFetching
              ? <Alert color="primary" style={{marginTop: '1.2rem'}}>Loading</Alert>
              : <Alert color="warning" style={{marginTop: '1.2rem'}}>Please Try To Loading Again</Alert>)
            : <Articles articles={articles} />
          }
        </Container>

        <Container>
          <Row style={{marginTop: '2.4rem', marginBottom: '4.8rem'}}>
            <Col>
              <Pagination value={selectedPage} onClick={this.handleChange} options={[...Array(10).keys()]} />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { selectedPage, articlesByPage } = state
  const {
    isFetching,
    items: articles
  } = articlesByPage[selectedPage] || {
    isFetching: true,
    items: []
  }

  return {
    selectedPage,
    articles,
    isFetching
  }
}

export default connect(mapStateToProps)(App)
