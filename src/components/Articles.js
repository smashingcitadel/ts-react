import React, { Component } from 'react'
import PropTypes from 'prop-types'

import {
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
  Button,
  Modal,
  ModalHeader} from 'reactstrap'

class Articles extends Component {
  constructor(props) {
    super(props)
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this)
    this.headline = ''
    this.snippet = ''
    this.mutimedia = ''
    this.source = ''
    this.pub = ''
  }

  toggle(article) {
    this.setState({modal: !this.state.modal})
    this.headline = article.headline !== undefined ? article.headline.main : ''
    this.snippet = article.snippet
    this.multimedia = (article.multimedia !== undefined && article.multimedia.length > 0) ? 'https://www.nytimes.com/' + article.multimedia[0].url : ''
    this.source = article.source
    this.pub = new Date(article.pub_date).toLocaleString('en-US')
  }

  render() {
    return (
      <div>
        {this.props.articles.map((article, index) =>
          <div key={index}>
            <Row style={{marginTop: '1.2rem'}}>
              <Col>
                <Card>
                  <CardBody>
                    <CardTitle>{article.headline.main}</CardTitle>
                    <CardText>{article.snippet}</CardText>
                    <Button outline color="primary" onClick={() => this.toggle(article)}>Details</Button>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </div>
        )}

        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader toggle={this.toggle}>{this.headline}</ModalHeader>
          <Card>
            <CardImg top width="100%" src={this.multimedia} alt="" />
            <CardBody>
              <CardText>{this.snippet}</CardText>
              <CardText>Source: {this.source}</CardText>
              <CardText><small>Date: {this.pub}</small></CardText>
            </CardBody>
          </Card>
        </Modal>
      </div>
    );
  }
}

Articles.propTypes = {
  articles: PropTypes.array.isRequired
}

export default Articles
