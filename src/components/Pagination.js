import React from 'react'
import PropTypes from 'prop-types'

import { Button, ButtonGroup, ButtonToolbar } from 'reactstrap'

const Header = ({ value, onClick, options }) => (
  <ButtonToolbar>
    <ButtonGroup>
      {options.map(option =>
        <Button
          key={option}
          onClick={e => onClick(e.target.value)}
          value={option}
          color="link" size="lg">
            {++option}
        </Button>
      )}
    </ButtonGroup>
  </ButtonToolbar>
)

Header.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.number.isRequired
  ).isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default Header
